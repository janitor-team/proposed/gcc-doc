gcc-5-doc (5.4.0-1) UNRELEASED; urgency=medium

  * New upstream release.
  * Synced patches with gcc-5, 5.4.1-8.
  * Build manpage for gcov-tool-5.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sun, 09 Apr 2017 20:54:53 -0400

gcc-5-doc (5.3.0-2) unstable; urgency=medium

  * Avoid using @value in @setfilename lines. (Closes: #815369)
  * Avoid underscore in texi2pdf output filename.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sat, 30 Apr 2016 18:13:23 -0400

gcc-5-doc (5.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Synced patches with gcc-5, 5.3.1-16.
  * Use https URIs for Vcs-*.
  * Bumped standards version to 3.9.7, no changes needed.
  * Fixed some undefined variables in gnat_rm.texi and gnat_ugn.texi.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Fri, 29 Apr 2016 01:50:52 -0400

gcc-5-doc (5.2.0-2) UNRELEASED; urgency=medium

  * Synced patches with gcc-5, 5.2.1-27.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Thu, 28 Apr 2016 21:34:30 -0400

gcc-5-doc (5.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Synced patches with gcc-5, 5.2.1-15, no changes needed.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Mon, 10 Aug 2015 14:34:26 -0400

gcc-5-doc (5.1.0-2) UNRELEASED; urgency=medium

  * Synced patches with gcc-5, 5.1.1-14, no changes needed.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sun, 09 Aug 2015 16:07:53 -0400

gcc-5-doc (5.1.0-1) unstable; urgency=low

  * New upstream branch. (Closes: #745968)
  * Adapt to new version number scheme.
  * Synced patches with gcc-5, 5.1.1-5.
  * Removed xgnatugn preprocessor, and dropped build-dep on gnat-*.
  * Include documentation for libgomp, libitm, libquadmath, and gccinstall
    in gcc-5-doc.
  * Updated copyright.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sun, 17 May 2015 20:09:08 -0400

gcc-4.9-doc (4.9.2-2) UNRELEASED; urgency=medium

  * Set Multi-Arch: foreign for all packages, to satisfy dependency of gcc-doc
    from other archs. (Closes: #766588)

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Thu, 30 Apr 2015 15:15:25 -0400

gcc-4.9-doc (4.9.2-1) unstable; urgency=medium

  * New upstream release: 4.9.2.
  * Synced patches with gcc-4.9, 4.9.2-10.
  * Use gnat-4.9 as the default version for gnat.
  * debian/extract-doc-tarball-from-upstream: generate xz by default.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Thu, 30 Apr 2015 14:44:39 -0400

gcc-4.9-doc (4.9.1-3) unstable; urgency=medium

  * Synced patches with gcc-4.9, 4.9.1-18, no changes needed.
  * Bumped standards version to 3.9.6, no changes needed.
  * Use texinfo.tex provided by texinfo.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Thu, 23 Oct 2014 01:24:04 -0400

gcc-4.9-doc (4.9.1-2) unstable; urgency=medium

  * Synced patches with gcc-4.9, 4.9.1-14, added new patches:
    - from-debian-gcc-svn-doc-updates.diff, and
    - from-debian-gcc-pr61294-doc.diff.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sat, 13 Sep 2014 16:06:11 -0400

gcc-4.9-doc (4.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Synced patches with gcc-4.9, 4.9.1-4, dropped
    from-debian-gcc-svn-doc-updates.diff.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sat, 02 Aug 2014 15:24:35 -0400

gcc-4.9-doc (4.9.0-3) UNRELEASED; urgency=medium

  * Synced patches with gcc-4.9, 4.9.0-11, no changes needed.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sat, 14 Jun 2014 03:51:41 -0400

gcc-4.9-doc (4.9.0-2) unstable; urgency=medium

  * Added a missed build-dep: texlive-fonts-recommended.
  * Synced patches with gcc-4.9, 4.9.0-5, no changes needed.
  * Build with texinfo >= 5.2.0.dfsg.1-3. (Closes: #749231)

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Wed, 04 Jun 2014 12:30:33 -0400

gcc-4.9-doc (4.9.0-1) unstable; urgency=medium

  * New upstream branch. (Closes: #745968)
  * Synced patches with gcc-4.9, 4.9.0-1. New patches:
    - from-debian-gcc-alpha-ieee-doc.diff
    - from-debian-gcc-rename-info-files.diff
    - from-debian-gcc-gdc-4.9-doc.diff
    - from-debian-gcc-svn-doc-updates.diff
  * Refreshed patches:
    - from-debian-gcc-gdc-4.9-doc.diff
    - fix-direntry.diff
    - gnat-cross-references.diff
  * Updated debian/copyright.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sun, 27 Apr 2014 16:06:38 -0400

gcc-4.8-doc (4.8.2-3) UNRELEASED; urgency=medium

  * Synced patches with gcc-4.8, 4.8.2-21, no changes needed.

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Sat, 01 Mar 2014 18:29:08 -0500

gcc-4.8-doc (4.8.2-2) unstable; urgency=low

  * Updated my uid.
  * Updated debhelper compatibility level from 7 to 9.
  * Added git branch in Vcs-Git.
  * Bumped standards version to 3.9.5, no changes needed.
  * Synced patches with gcc-4.8, 4.8.2-8, no changes needed.
  * Fixed the description of gnat-4.8-doc. (Closes: #730693)

 -- Guo Yixuan (郭溢譞) <culu.gyx@gmail.com>  Wed, 11 Dec 2013 19:07:44 -0500

gcc-4.8-doc (4.8.2-1) unstable; urgency=low

  * New upstream release.
  * Synced patches with gcc-4.8, 4.8.2-1.

 -- Guo Yixuan <culu.gyx@gmail.com>  Wed, 23 Oct 2013 22:14:34 -0400

gcc-4.8-doc (4.8.1-2) UNRELEASED; urgency=low

  * Synced patches with gcc-4.8, 4.8.1-10.
  * Dropped fix-value-gcc.diff.

 -- Guo Yixuan <culu.gyx@gmail.com>  Wed, 03 Jul 2013 14:56:31 +0800

gcc-4.8-doc (4.8.1-1) unstable; urgency=low

  * New upstream release.
  * Synced patches with gcc-4.8, 4.8.1-3.

 -- Guo Yixuan <culu.gyx@gmail.com>  Wed, 19 Jun 2013 00:02:38 +0800

gcc-4.8-doc (4.8.0-2) UNRELEASED; urgency=low

  * Synced patches with gcc-4.8, 4.8.0-7, no changes needed.

 -- Guo Yixuan <culu.gyx@gmail.com>  Tue, 21 May 2013 10:00:22 +0800

gcc-4.8-doc (4.8.0-1) unstable; urgency=low

  * New upstream branch. (Closes: #707673)
  * debian/patches:
    - Synced patches with gcc-4.8, 4.8.0-5.
    - Dropped alpha-ieee-doc-clarification.diff and gccgo-direntry.diff,
      which is already in from-debian-gcc-alpha-ieee-doc.diff and
      from-debian-gcc-rename-info-files.diff.
    - Refreshed fix-direntry.diff.
    - fix-value-gcc.diff to correct variable names.
  * debian/control:
    - start building gnat-4.8,
    - stop building gcc-doc-base
    - Removed Samuel from uploaders.

 -- Guo Yixuan <culu.gyx@gmail.com>  Fri, 10 May 2013 15:22:35 +0800

gcc-4.7-doc (4.7.2-3) UNRELEASED; urgency=low

  * Fixed gccgo's info direntry.

 -- Guo Yixuan <culu.gyx@gmail.com>  Sun, 20 Jan 2013 22:36:53 +0800

gcc-4.7-doc (4.7.2-2) unstable; urgency=low

  * New maintainer.
  * Thanks to Samuel Bronson for his work on this package. I kept him in
    uploaders as credit.
  * Removed DMUA flag, as it's being deprecated. Thanks to Steffen for
    adding that.
  * Improved README.source, and added a helper script, check_gcc_patches.
  * debian/patches:
    - Synced patches with gcc-4.7, 4.7.2-4.
      - updated debian/patches/from-debian-gcc-rename-info-files.diff
    - Added a patch, alpha-ieee-doc-clarification.diff, to state that
      changes in from-debian-gcc-alpha-ieee-doc.diff are specific to the alpha
      architecture.
    - Added fix-direntry.diff, to correct links for the info directory
      file, and gnat-cross-references.diff, to have correct cross references.
  * Updated copyright.
  * Removed gnat-4.7-doc, as gnat-4.7 is not in Debian.

 -- Guo Yixuan <culu.gyx@gmail.com>  Tue, 30 Oct 2012 14:12:57 +0800

gcc-4.7-doc (4.7.2-1) unstable; urgency=low

  [Guo Yixuan]
  * Updated and sync patches with gcc-4.7 4.7.2-2. (Closes: #689211)
    + All *.texi files from upstream are added, so more documents can be
      built if needed.
  * Add myself to uploaders.
  * Bumped policy version to 3.9.4.
  * Added texlive-latex-recommended in build-deps to fix build.
  * Fixed spaces in debian/rules.
  * Updated git repo urls in Vcs-*.
  [Steffen Moeller (sponsor)]
  * DMUA flag added, Guo Yixuan is known to sponsor.

 -- Guo Yixuan <culu.gyx@gmail.com>  Sun, 30 Sep 2012 19:50:44 +0800

gcc-4.7-doc (4.7.0-1~naesten1) unstable; urgency=low

  * Package documentation for GCC & friends version 4.7.0.
  * Sync patches with gcc-4.7 version 4.7.0-1.
    + Had to refresh patches to get them all to apply; some were fuzzy.

 -- Samuel Bronson <naesten@gmail.com>  Tue, 03 Apr 2012 18:06:08 -0400

gcc-4.6-doc (4.6.3-1~naesten8) unstable; urgency=low

  * Build PDFs, at Robert Wotzlaw's suggestion and using parts of his changes.
    (Perhaps "inspired by" might be more accurate.)
    + Instead of hard-coding the filenames into the xrefs, which would
      (a) require rewriting rename-info-files.patch from the gcc-X.Y package and
      (b) preclude using different filenames for different output formats,
      we "simply" add support for @value in xref filenames to texinfo.tex.
      Thanks to tex.stackexchange.com for the TeX hints and to Karl Berry for
      accepting my patch.
    + Add the PDFs to our .docs and .doc-base* files, so they get installed and
      registered as alternate forms of the relevant documents.
    + Instruct dh_compress not to compress them, because that often leads
      to trouble with browsers and/or web servers.

 -- Samuel Bronson <naesten@gmail.com>  Tue, 03 Apr 2012 13:31:21 -0400

gcc-4.6-doc (4.6.3-1~naesten7) unstable; urgency=low

  * Fix the links between HTML manuals.
    + Use /usr/share/doc/gcc-X.Y-doc for all packages except gcc-doc-base,
      since we can't use @xref between directories.
    + Stop linking to (e.g.) gcc-4.6.html; the file is named gcc.html!
    + Update the *.doc-base* files for the new paths.
  * Unfortunately, many cross-references involving the GNAT *info* files
    have been broken for a while (probably at least since the docs got
    kicked out of gcc-X.Y proper for being non-free).  (This is arguably a
    bug in the rename-info-files.patch from the gcc-X.Y packages.)

 -- Samuel Bronson <naesten@gmail.com>  Fri, 30 Mar 2012 19:14:37 -0400

gcc-4.6-doc (4.6.3-1~naesten6) unstable; urgency=low

  * New upstream release.
  * New source package name.
  * Sync with gcc-4.6 version 4.6.3-1.

 -- Samuel Bronson <naesten@gmail.com>  Thu, 29 Mar 2012 12:49:01 -0400

gcc-4.6-doc-non-dfsg (4.6.2-1~naesten5) UNRELEASED; urgency=low

  * Build gnat_ugn using the "xgnatugn" preprocessor, like upstream does.
    + Update debian/extract-doc-tarball-from-upstream to include the
      needed files in .orig tarballs.
  * Simplify README.source using the "get-orig-source" rule, and assuming
    that anyone trying to update the package to a new upstream version
    will be working in git.
  * debian/rules, debian/extract-doc-tarball-from-upstream:
    + Update for impending rename (of source package) to gcc-X.Y-doc.
    + Set mtime=0 in gzip header, so tarballs are reproducible.

 -- Samuel Bronson <naesten@gmail.com>  Sun, 04 Mar 2012 14:32:51 -0500

gcc-4.6-doc-non-dfsg (4.6.2-1~naesten4) unstable; urgency=low

  * debian/copyright:
    + Switch to new URL for copyright format.
  * debian/control:
    + Drop the Conflicts and Replaces fields, since even lenny doesn't
      have any of the affected package versions.
    + Update Standards-Version to 3.9.3.
    + Add Vcs-* fields.
    + Set myself as Maintainer, remove Uploaders field.

 -- Samuel Bronson <naesten@gmail.com>  Tue, 28 Feb 2012 16:32:28 -0500

gcc-4.6-doc-non-dfsg (4.6.2-1~naesten3) unstable; urgency=low

  * README.source: Replace the paragraph about dpatch with one about
    "3.0 (quilt)".
  * Simplify "new X.Y version" procedure.

 -- Samuel Bronson <naesten@gmail.com>  Tue, 14 Feb 2012 20:47:38 -0500

gcc-4.6-doc-non-dfsg (4.6.2-1~naesten2) unstable; urgency=low

  * Update /debian/watch to check for 4.6 releases.
  * Implement `debian/rules get-orig-source', more or less.
  * Take a stab at converting debian/copyright to DEP5 format.
  * New package "gccgo" for the Go compiler manual.

 -- Samuel Bronson <naesten@gmail.com>  Mon, 13 Feb 2012 17:48:06 -0500

gcc-4.6-doc-non-dfsg (4.6.2-1~naesten1) unstable; urgency=low

  * Packaged documentation files for gcc, gnat and gcj version 4.6.2.
    Closes: #656044 (RFP for gcc-4.6-doc).

 -- Samuel Bronson <naesten@gmail.com>  Sat, 21 Jan 2012 18:16:21 -0500

gcc-4.5-doc-non-dfsg (4.5.3-1) unstable; urgency=low

  * Bump version to a normal Debian version number, on the theory that the
    package is ready for upload.

 -- Samuel Bronson <naesten@gmail.com>  Sat, 21 Jan 2012 00:25:13 -0500

gcc-4.5-doc-non-dfsg (4.5.3-1~naesten4) unstable; urgency=low

  * Add a debian/watch file.
  * Update README.source for the new source format, and add "update the
    watch file" to the "new X.Y" steps.

 -- Samuel Bronson <naesten@gmail.com>  Thu, 19 Jan 2012 12:28:02 -0500

gcc-4.5-doc-non-dfsg (4.5.3-1~naesten3) unstable; urgency=low

  * Don't try to use debian/compat of "9" yet, since apparently lintian
    won't be happy with that until debhelper >= 9 is out.

 -- Samuel Bronson <naesten@gmail.com>  Wed, 18 Jan 2012 20:30:35 -0500

gcc-4.5-doc-non-dfsg (4.5.3-1~naesten2) unstable; urgency=low

  * Converted to "3.0 (quilt)" format.
    + Updated debian/convert-debian-gcc-diff script accordingly.
  * Switched to dh(1) based build.
  * Bumped Standards-Version to 3.9.2.

 -- Samuel Bronson <naesten@gmail.com>  Wed, 18 Jan 2012 18:04:12 -0500

gcc-4.5-doc-non-dfsg (4.5.3-1~naesten1) unstable; urgency=low

  * Packaged documentation for gcc, gnat, and gcj version 4.5.3.
  * Use new script debian/extract-doc-tarball-from-upstream to generate
    .orig.tar.gz file.
    + Move texi2pod.pl to contrib/texi2pod.pl, where it appears upstream,
      to avoid having to special-case it.

 -- Samuel Bronson <naesten@gmail.com>  Mon, 16 Jan 2012 16:54:25 -0500

gcc-4.4-doc-non-dfsg (4.4.4.nf1-1) unstable; urgency=low

  * Packaged documentation files for gcc, gnat and gcj version 4.4.4.
  * Updated Standards-Version to 3.8.4.

 -- Nikita V. Youshchenko <yoush@debian.org>  Mon, 03 May 2010 10:29:56 +0400

gcc-4.3-doc-non-dfsg (4.3.2.nf1-1) unstable; urgency=low

  * Rebuilt .orig.tar.gz using files from gcc-4.3.2 release.
    + Removed treelang documentation from .orig.tar.gz - it is no longer
      needed.
    + gpl.7 manual page now contains GPL3 text.
  * Resynced debian patches against version 4.3.2-1 of gcc-4.3 package.
  * Updated Standards-Version to 3.8.0.
    + Added debian/README.source with some packaging documentation. 
    + Added support for 'parallel=n' in DEB_BUILD_OPTIONS.
    + Added a note to debian/copyright explaining why this package
      is not part of debian main.

 -- Nikita V. Youshchenko <yoush@debian.org>  Sun, 28 Sep 2008 20:00:54 +0400

gcc-4.3-doc-non-dfsg (4.3.0.nf1-1) unstable; urgency=low

  * Packaged documentation files for gcc, gnat and gcj version 4.3.0.
  * Removed treelang documentation package since treelang is no longer
    in Debian.
  * Mentioned AdaCore copyright on Ada documentation in debian/copyright.

 -- Nikita V. Youshchenko <yoush@debian.org>  Tue,  3 Jun 2008 23:37:00 +0400

gcc-4.2-doc-non-dfsg (4.2.3.nf1-1) unstable; urgency=low

  * Packaged documentation files for gcc and gnat version 4.2.3.
  * Added gccinstall info document, since it is referenced by
    other info documents.
  * Use debian/compat instead of DH_COMPAT in debian/rules.
  * Removed Build-Depends: conent from Build-Depends-Indep.
  * Bumped Standards-Version to 3.7.3.

 -- Nikita V. Youshchenko <yoush@debian.org>  Sat,  9 Feb 2008 23:36:50 +0300

gcc-4.1-doc-non-dfsg (4.1.2.nf1-1) unstable; urgency=low

  * Resynced against upstream gcc 4.1.2 and debian gcc 4.1.2-12
    (but excluded libjava-backport-doc-updates2.dpatch that does not apply).
  * Fixed typo in gcc-doc-base package description. Closes: #404308.
  * Added ada documentation package. Closes: #418635. Thanks to
    Michael Bode <michael_bode@gmx.net> for the patch.
  * Still no java documentation, sorry. Patches welcome :).
  * Register treelang documentation in doc-base.

 -- Nikita V. Youshchenko <yoush@debian.org>  Tue, 19 Jun 2007 00:48:09 +0400

gcc-4.1-doc-non-dfsg (4.1.1.nf3-1) unstable; urgency=high

  * Rebiuld .orig.tar.gz, to include gpl.7, gfdl.7, and fsf-funding.7
    man pages.
  * New binary package gcc-doc-base that provides these manpages.
    Closes: #389075.
  * Resync debian gcc patches, using gcc-4.1 4.1.1ds1-15.
    Closes: #389076.
    However, documentation will eventially become out of sync, so
    resync should be done after each gcc-4.1 package upload.
  * Set urgency to high, because etch currently does not have
    gcc docs at all, and this does hurt people.
  * Enough for today :).

 -- Nikita V. Youshchenko <yoush@debian.org>  Mon,  2 Oct 2006 23:49:27 +0400

gcc-4.1-doc-non-dfsg (4.1.1.nf2-1) unstable; urgency=low

  * Redone packaging, so man pages are also built from source.
  * Fixed package descriptions to mention that documents are not just GFDL,
    but contain invariant sections.
  * Added script to convert patches from debian gcc package to form
    suitable for this package.
  * Using that script, added patches from debian gcc-4.1 4.1.1ds1-14
  * Those patches superseed pathes that I've manually added in the
    previous version of the package, so removed my versions.
  * Added preinst scripts that remove old symlink to doc/gcc-4.1-base
    (Closes: #390093).
  * Not adding more documentation packages in this release, so upload
    will not get to NEW.

 -- Nikita V. Youshchenko <yoush@debian.org>  Sun,  1 Oct 2006 12:56:14 +0400

gcc-4.1-doc-non-dfsg (4.1.1-nf1) unstable; urgency=low

  * Initial release, based in files from gcc-4.1.1 tarball.
  * Version is numbered in such way to make transparent updates from
    previous gcc documentation packages.

 -- Nikita V. Youshchenko <yoush@debian.org>  Thu, 21 Sep 2006 00:04:14 +0400
